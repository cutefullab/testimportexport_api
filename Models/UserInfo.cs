namespace testImportExport_api.Models
{
    public class UserInfo
    {
        public string UserName { get; set; }

        public int Age { get; set; }
    }
}