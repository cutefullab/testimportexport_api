using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using testImportExport_api.Models;

namespace ManageFile
{
    [ApiController]
    [Route("api")]
    public class ManageFileController : Controller
    {

        ILogger<ManageFileController> _logger;

        public ManageFileController(ILogger<ManageFileController> logger)
        {
            _logger = logger;
        }

        [HttpGet("export")]
        public async Task<IActionResult> Export(CancellationToken cancellationToken)
        {
            // query data from database
            await Task.Yield();
            var list = new List<UserInfo>()
            {
                new UserInfo { UserName = "Cherprang48", Age = 23 },
                new UserInfo { UserName = "Minmin48", Age = 21 },
                new UserInfo { UserName = "Mewnich48", Age = 17 },
            };

            var list2 = new List<UserInfo>()
            {
                new UserInfo { UserName = "xx48", Age = 48 },
                new UserInfo { UserName = "xxx48", Age = 47 },
                new UserInfo { UserName = "xxxx48", Age = 46 },
            };

            var stream = new MemoryStream();

            using (var package = new ExcelPackage(stream))
            {
                var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                workSheet.Cells.LoadFromCollection(list, true);

                var workSheet2 = package.Workbook.Worksheets.Add("Sheet2");
                workSheet2.Cells.LoadFromCollection(list2, true);

                package.Save();
            }
            stream.Position = 0;
            string excelName = $"UserList-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";

            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", excelName);
        }

        [HttpPost("import")]
        public async Task<DemoResponse<List<UserInfo>>> Import(IFormFile formFile, CancellationToken cancellationToken)
        {
            if (formFile == null || formFile.Length <= 0)
            {
                return DemoResponse<List<UserInfo>>.GetResult(-1, "formfile is empty");
            }

            if (!Path.GetExtension(formFile.FileName).Equals(".xlsx", StringComparison.OrdinalIgnoreCase))
            {
                return DemoResponse<List<UserInfo>>.GetResult(-1, "Not Support file extension");
            }

            var list = new List<UserInfo>();

            using (var stream = new MemoryStream())
            {
                await formFile.CopyToAsync(stream, cancellationToken);

                using (var package = new ExcelPackage(stream))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                    var rowCount = worksheet.Dimension.Rows;

                    for (int row = 2; row <= rowCount; row++)
                    {
                        list.Add(new UserInfo
                        {
                            UserName = worksheet.Cells[row, 1].Value.ToString().Trim(),
                            Age = int.Parse(worksheet.Cells[row, 2].Value.ToString().Trim()),
                        });
                    }
                }
            }

            // add list to db ..
            // here just read and return

            return DemoResponse<List<UserInfo>>.GetResult(0, "OK", list);
        }
    }
}